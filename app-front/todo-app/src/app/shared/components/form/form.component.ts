import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Item } from '../../../shared/model/item/item';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  item : Item;
@Output() messageEvent = new EventEmitter<Item>();

  constructor() { }

  ngOnInit() {
  }

}
