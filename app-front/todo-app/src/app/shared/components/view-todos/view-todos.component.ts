import { Component, OnInit } from '@angular/core';
import { Item } from '../../../shared/model/item/item';
import { ItemService } from 'src/app/core/services/item/item.service';

@Component({
  selector: 'app-view-todos',
  templateUrl: './view-todos.component.html',
  styleUrls: ['./view-todos.component.css']
})
export class ViewTodosComponent implements OnInit {
  items:Item[];
  constructor(private itemService : ItemService) { }

  ngOnInit() {
    this.getProducts();
  }
  getProducts() {
    this.items=[];
     this.itemService.getItems().subscribe( data => {
       console.log(data);   
       this.items=data;      
      });     
      }
}
