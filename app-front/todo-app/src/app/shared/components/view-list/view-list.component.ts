import { Component, OnInit } from '@angular/core';
import { Item } from '../../../shared/model/item/item';
import { ItemService } from 'src/app/core/services/item/item.service';
import { LoginService } from 'src/app/core/authentication/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-list',
  templateUrl: './view-list.component.html',
  styleUrls: ['./view-list.component.css']
})
export class ViewListComponent implements OnInit {
  items:Item[];
  user : firebase.User;
  constructor(private itemService : ItemService,
    private service: LoginService,
    private router: Router) { }

  ngOnInit() {
    this.service.getLoggedInUser()
    .subscribe(user => {     
      this.user = user;      
      if(!user){    
        console.log(user);    
        this.router.navigate(['/login']);
      }
    })
    this.getProducts();
  }

  getProducts() {
    this.items;
     this.itemService.getExternalItems().subscribe( data => {
       console.log(data);   
       this.items=data;      
      });     
      }

}
