import { Component, OnInit } from '@angular/core';
import { Item } from '../../../shared/model/item/item';
import { ItemService } from 'src/app/core/services/item/item.service';
import * as M from '../../../../assets/materialize/js/materialize.min.js';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent implements OnInit {

  item : Item;
  response : Item;
  constructor(private itemService : ItemService,private router: Router) { }

  ngOnInit() {
  }
  updateItem(idItem: number, titulo:string){  
    this.item = new Item();
    this.item.id = idItem;
    this.item.title = titulo;
    
    this.itemService.updateItem(this.item).subscribe(data => {     
     this.response=data;
     if(data){
       M.toast({html: "Item modificado correctamente"},4000);
       this.router.routeReuseStrategy.shouldReuseRoute = function () {
        return false;
      };
       this.router.navigate(['/editar']);
     }       
    },(err) => { M.toast({html: "No se pudo actualizar el item"},4000);});
    }

}
