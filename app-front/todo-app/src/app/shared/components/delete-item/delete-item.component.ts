import { Component, OnInit } from '@angular/core';
import { Item } from '../../../shared/model/item/item';
import { ItemService } from 'src/app/core/services/item/item.service';
import * as M from '../../../../assets/materialize/js/materialize.min.js'
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-item',
  templateUrl: './delete-item.component.html',
  styleUrls: ['./delete-item.component.css']
})
export class DeleteItemComponent implements OnInit {
  item: Item[];
  constructor(private itemService : ItemService,private router: Router) { }

  ngOnInit() {
  }

  deleteItem(id:number){  
    this.itemService.deleteItem(id).subscribe( data => {
      M.toast({html: "Item eliminado correctamente"},4000, 'blue');
      this.router.routeReuseStrategy.shouldReuseRoute = function () {
        return false;
      };
      this.router.navigate(['/eliminar']);    
     });
    }

}
