import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/core/authentication/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {

  constructor(private service: LoginService,
    private router: Router) { }

  ngOnInit() {
  }

  singUp(user:string, pass:string){
    console.log('Resgistrando usuario');
    this.service.singUp(user,pass);
    if(user){    
      console.log(user);    
      this.router.navigate(['external']);      
    }
  }

}
