import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../../shared/model/item/item';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Input() items: Item[];
  itemsCompleta: Item[] = this.items;
  itemsFiltro: Item[];

  constructor() {
  }
  ngOnChanges(){
    this.itemsCompleta=[];
    this.itemsCompleta = this.items;
    console.log(this.itemsCompleta);
  }

  ngOnInit() {
   
  }

  filtrarTitulo(titulo: string) {
    if (titulo) {
      const itemsArray: any[] = [];
      titulo = titulo.toLowerCase();
      for (const item of this.items) {
        const tituloItem = item.title.toLowerCase();
        if (tituloItem.indexOf(titulo) >= 0) {
          itemsArray.push(item);
        }
      }
      this.itemsCompleta = itemsArray;
    } else {
      this.itemsCompleta = this.items;
    }
  }

  filtrarId(id: number) {
    if (id) {
      const itemsArray: any[] = [];     
      for (const item of this.items) {
        const idItem = item.id;
        if (idItem.valueOf() == id) {
          itemsArray.push(item);
        }
      }
      this.itemsCompleta = itemsArray;
    } else {
      this.itemsCompleta = this.items;
    }
  }

}
