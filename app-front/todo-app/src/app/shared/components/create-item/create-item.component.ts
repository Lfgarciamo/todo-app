import { Component, OnInit,ViewChild, AfterViewInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ItemService } from 'src/app/core/services/item/item.service';
import { Item } from '../../../shared/model/item/item';
import {FormComponent} from '../form/form.component'
import * as M from '../../../../assets/materialize/js/materialize.min.js'
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.css']
})
export class CreateItemComponent implements OnInit {
  
  item : Item;
  response : Item;
  idItem:number;
  constructor(private itemService : ItemService,
    private router: Router) { }
  ngOnInit() {
   
  }

  addItem(idItem: number, titulo:string){
  
   this.item = new Item();
   this.item.id = idItem;
   this.item.title = titulo;
   
   this.itemService.addItem(this.item).subscribe(data => {     
    this.response=data;
    if(data){
      M.toast({html: "Item agregado correctamente"},4000);
      this.router.routeReuseStrategy.shouldReuseRoute = function () {
        return false;
      };
      this.router.navigate(['/crear']);
    }       
   },(err) => { M.toast({html: "No se pudo agregar el item"},4000);});
   }

  }

