import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/core/authentication/login.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import * as M from '../../../../assets/materialize/js/materialize.min.js'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user : firebase.User;
  constructor(private afAuth: AngularFireAuth,
    private service: LoginService,
    private router: Router) { }
  ngOnChanges(){
    M.updateTextFields();   
    this.service.getLoggedInUser()
    .subscribe(user => {     
      this.user = user;      
      if(user){    
        console.log(user);    
        this.router.navigate(['/external']);
      }else{
        this.router.navigate(['/login']);
      }
    })
  }

  ngOnInit() {
   
  }

  logIn(user:string, pass:string){
    console.log('Iniciando sesion');
    this.service.login(user,pass);
    if(user){    
      console.log(user);    
      this.router.navigate(['external']);
    }
  }

 
}
