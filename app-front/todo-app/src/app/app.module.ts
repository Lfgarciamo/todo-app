import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { environment } from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './shared/components/login/login.component';
import { HeaderComponent } from './core/header/header.component';
import { RouterModule, Routes } from '@angular/router';
import { CreateItemComponent } from './shared/components/create-item/create-item.component';
import { ViewListComponent } from './shared/components/view-list/view-list.component';
import { HttpClientModule } from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';
import { ListComponent } from './shared/components/list/list.component';
import { FormComponent } from './shared/components/form/form.component';
import { EditItemComponent } from './shared/components/edit-item/edit-item.component';
import {FormsModule} from '@angular/forms';
import { DeleteItemComponent } from './shared/components/delete-item/delete-item.component';
import { ViewTodosComponent } from './shared/components/view-todos/view-todos.component';
import { SingupComponent } from './shared/components/singup/singup.component';


const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  {path : 'external', component : ViewListComponent},
  {path: 'crear', component: CreateItemComponent},
  {path: 'editar', component: EditItemComponent},
  {path: 'eliminar', component: DeleteItemComponent},
  {path: 'consultar', component: ViewTodosComponent},
  {path: 'singup', component: SingupComponent}
  
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    CreateItemComponent,
    ViewListComponent,   
    ListComponent, FormComponent, EditItemComponent, DeleteItemComponent, ViewTodosComponent, SingupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.fireBase),
    AngularFireAuthModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true,onSameUrlNavigation: 'reload' }
    ),
    HttpClientModule,
    NgxPaginationModule,
    FormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
