import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as M from '../../../assets/materialize/js/materialize.min.js'
import { AngularFireAuth } from '@angular/fire/auth';
import { LoginService } from '../../core/authentication/login.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
   
  user : firebase.User;
  exist : boolean = false;
  constructor(private afAuth: AngularFireAuth,
    private loginService : LoginService,
    private router: Router) { }

    ngOnChanges(){
      
    }
    
  ngOnInit() { 
    var elems = document.querySelectorAll('.fixed-action-btn');
    var instances = M.FloatingActionButton.init(elems, {});

    var elems = document.querySelectorAll('.dropdown-trigger');
  var instances = M.Dropdown.init(elems, {
    constrainWidth:false,
    alignment:'right'
  });
    this.loginService.getLoggedInUser()
    .subscribe(user => {
      console.log(user);
      this.user = user;
      this.exist= true;
      if(!user){
        this.exist= false;
        this.router.navigate(['/login']);
      }else{this.router.navigate(['/external']);}
    });
     
  }

  logOut(){
    this.loginService.loggOut();
  }
}
