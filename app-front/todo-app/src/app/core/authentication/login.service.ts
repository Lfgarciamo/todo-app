import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private afAuth: AngularFireAuth) { }

  login(user:string, pass:string){
    console.log('redirigiendo a el login'+ user);
   this.afAuth.auth.signInWithEmailAndPassword(user,pass);
  }
  getLoggedInUser(){
    return this.afAuth.authState;
  }

  loggOut(){
    this.afAuth.auth.signOut();
  }
  singUp(email:string, pass:string){
    this.afAuth.auth.createUserWithEmailAndPassword(email,pass);
  }
}
