import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Item } from '../../../shared/model/item/item';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import * as M from '../../../../assets/materialize/js/materialize.min.js'

const endpoint = 'https://buscatucolegio.com/todo-app/v1';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private http: HttpClient) { }
  getExternalItems() {
    return this.http.get<Item[]>(endpoint + '/external/items');
}
getItems() {
  return this.http.get<Item[]>(endpoint + '/items');
}

addItem (item: Item) {  
  return this.http.post<Item>(endpoint+"/items", item, httpOptions)
    .pipe(
      catchError(this.handleError)
    );
}

updateItem (item: Item) {  
  return this.http.put<Item>(endpoint+"/items/"+item.id, item, httpOptions)
    .pipe(
      catchError(this.handleError)
    );
}

deleteItem (id: number) {  
  return this.http.delete(endpoint+"/items/"+id);
}

handleError(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
  // Get client-side error
  errorMessage = error.error.message;
  } else {
  // Get server-side error
  errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  return throwError(errorMessage);  
  }
}
