// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  fireBase:{  
      apiKey: "AIzaSyDbq6sN4afpsYJ1mO6EIt5oV5kLqEKVaY0",
      authDomain: "todo-itau.firebaseapp.com",
      databaseURL: "https://todo-itau.firebaseio.com",
      projectId: "todo-itau",
      storageBucket: "",
      messagingSenderId: "280149912035",
      appId: "1:280149912035:web:47563b116562cbcd"  
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
