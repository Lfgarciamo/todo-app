package com.itau.prueba.todoservices.common.dtos;

import com.itau.prueba.todoservices.common.model.ToDo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ToDoDto  {
    private String key;
    private long id;
    private String title;
}
