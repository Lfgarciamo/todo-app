package com.itau.prueba.todoservices.common.enums;

public enum ErrorCodes {
    GENERIC_ERROR ("G-001","Error generico");

    private String message;
    private String description;
    ErrorCodes(String message, String description) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
}
