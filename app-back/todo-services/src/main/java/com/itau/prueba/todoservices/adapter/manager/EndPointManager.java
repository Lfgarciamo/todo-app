package com.itau.prueba.todoservices.adapter.manager;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

public class EndPointManager implements IEndPointManger {

    @Override
    public ResponseEntity endpointConsumerClient(String pathEndpoint, Class<?> typeResponse, HttpMethod method, HashMap<String, String> headers) {

        RestTemplate clientConsumer = new RestTemplate();
        HttpHeaders httpHeadersConsumer = new HttpHeaders();
        httpHeadersConsumer.setAll(headers);
        return clientConsumer.exchange(pathEndpoint, method, new HttpEntity<>(httpHeadersConsumer), typeResponse);
    }


}
