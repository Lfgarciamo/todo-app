package com.itau.prueba.todoservices.repository.todo;

import com.itau.prueba.todoservices.common.dtos.ToDoDto;
import com.itau.prueba.todoservices.common.model.ToDo;
import java.util.List;

public interface IToDoRepository {
    List<ToDoDto> getTodoById(long id) throws InterruptedException;
    List<ToDoDto> createTodo(ToDo item) throws Exception;
    List<ToDoDto> updateTodoById(long id,ToDo updated) throws Exception;
    List<ToDoDto> getAllToDos();
    void deleteTodoById(long id) throws Exception;
}
