package com.itau.prueba.todoservices.controller.api.rest.todo;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.*;
import com.itau.prueba.todoservices.adapter.apis.todo.IToDoApiClient;
import com.itau.prueba.todoservices.common.dtos.ToDoDto;
import com.itau.prueba.todoservices.common.model.ToDo;
import com.itau.prueba.todoservices.services.todo.IManageToDo;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("todo-app/v1")
public class TodoApiImpl implements ITodoApi {

    final private IToDoApiClient iToDoApiClient;

    final private IManageToDo iManageToDo;

    @Autowired
    public TodoApiImpl(IToDoApiClient iToDoApiClient, IManageToDo iManageToDo) {
        this.iManageToDo = iManageToDo;
        this.iToDoApiClient = iToDoApiClient;
    }

    @GetMapping("/external/items")
    @Override
    public ResponseEntity viewData() {
        return iToDoApiClient.viewData();
    }

    @GetMapping("/items/{id}")
    @Override
    public ResponseEntity getTodoById( @PathVariable("id") long id) {

             return iManageToDo.getTodoById(id);
    }

    @PostMapping("/items")
    @Override
    public ResponseEntity createTodo(@RequestBody ToDo item) {
        return iManageToDo.createTodo(item);
    }
    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/items/{id}")
    @Override
    public ResponseEntity updateTodoById(@RequestBody ToDo item,@PathVariable("id") long id)
    {
        return iManageToDo.updateTodoById(item, id);
    }
    @DeleteMapping("/items/{id}")
    @Override
    public ResponseEntity deleteTodoById(@PathVariable("id") long id) {
        return iManageToDo.deleteTodoById(id);
    }

    @GetMapping("/items")
    @Override
    public ResponseEntity getAllToDos() {
        return iManageToDo.getAllToDos();
    }
}
