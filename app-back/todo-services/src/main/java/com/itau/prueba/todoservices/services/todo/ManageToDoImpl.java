package com.itau.prueba.todoservices.services.todo;

import com.itau.prueba.todoservices.adapter.apis.todo.ToDoApiClientImpl;
import com.itau.prueba.todoservices.common.dtos.ToDoDto;
import com.itau.prueba.todoservices.common.enums.ErrorCodes;
import com.itau.prueba.todoservices.common.model.ErrorMessage;
import com.itau.prueba.todoservices.common.model.ToDo;
import com.itau.prueba.todoservices.repository.todo.IToDoRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Service
public class ManageToDoImpl implements IManageToDo{

    private final IToDoRepository iToDoRepository;
    private static final Logger log = LogManager.getLogger(ManageToDoImpl.class);

    @Autowired
    public ManageToDoImpl(IToDoRepository iToDoRepository) {
        this.iToDoRepository = iToDoRepository;
    }

    @Override
    public ResponseEntity getTodoById(long id) {

        if (ObjectUtils.isEmpty(id)){
            String uid = UUID.randomUUID().toString();
            log.error("ManageToDoImpl | getTodoById | traceID | " + uid);
            log.error("Error | " + "El parametro id es nulo o vacio");
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST.value())
                    .body(ErrorMessage.builder()
                            .code(ErrorCodes.GENERIC_ERROR.getMessage())
                            .exceptionType("N/A")
                            .message("El parametro id es nulo o vacio")
                            .traceID(uid)
                            .build());

        }
        try{
            List<ToDoDto> a = iToDoRepository.getTodoById(id);
            if (ObjectUtils.isEmpty(a)){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
            return ResponseEntity.ok(a);
        }
        catch (Exception e) {
            String uid = UUID.randomUUID().toString();
            log.error("ManageToDoImpl | getTodoById | traceID | " + uid);
            log.error("Exception | " + e);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .body(ErrorMessage.builder()
                            .code(ErrorCodes.GENERIC_ERROR.getMessage())
                            .exceptionType(e.getClass().toString())
                            .message(e.getMessage())
                            .traceID(uid)
                            .build());
        }

    }

    @Override
    public ResponseEntity createTodo(ToDo item) {
        if (ObjectUtils.isEmpty(item)){
            String uid = UUID.randomUUID().toString();
            log.error("ManageToDoImpl | createTodo | traceID | " + uid);
            log.error("Error | " + "El parametro item es nulo o vacio");
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST.value())
                    .body(ErrorMessage.builder()
                            .code(ErrorCodes.GENERIC_ERROR.getMessage())
                            .exceptionType("N/A")
                            .message("El parametro item es nulo o vacio")
                            .traceID(uid)
                            .build());

        }
        try{
            List<ToDoDto> a = iToDoRepository.createTodo(item);
            return ResponseEntity.ok(a);
        }
        catch (Exception e) {
            String uid = UUID.randomUUID().toString();
            log.error("ManageToDoImpl | createTodo | traceID | " + uid);
            log.error("Exception | " + e);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .body(ErrorMessage.builder()
                            .code(ErrorCodes.GENERIC_ERROR.getMessage())
                            .exceptionType(e.getClass().toString())
                            .message(e.getMessage())
                            .traceID(uid)
                            .build());
        }

    }

    @Override
    public ResponseEntity updateTodoById(ToDo item, long id) {

        if (ObjectUtils.isEmpty(item) || ObjectUtils.isEmpty(id)){
            String uid = UUID.randomUUID().toString();
            log.error("ManageToDoImpl | updateTodoById | traceID | " + uid);
            log.error("Error | " + "El parametro item es nulo o vacio");
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST.value())
                    .body(ErrorMessage.builder()
                            .code(ErrorCodes.GENERIC_ERROR.getMessage())
                            .exceptionType("N/A")
                            .message("El parametro item es nulo o vacio")
                            .traceID(uid)
                            .build());

        }
        try{
            List<ToDoDto> a =  iToDoRepository.updateTodoById(id,item);
            if(ObjectUtils.isEmpty(a)){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
            return ResponseEntity.ok(a);
        }
        catch (Exception e) {
            String uid = UUID.randomUUID().toString();
            log.error("ManageToDoImpl | updateTodoById | traceID | " + uid);
            log.error("Exception | " + e);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .body(ErrorMessage.builder()
                            .code(ErrorCodes.GENERIC_ERROR.getMessage())
                            .exceptionType(e.getClass().toString())
                            .message(e.getMessage())
                            .traceID(uid)
                            .build());
        }

    }

    @Override
    public ResponseEntity deleteTodoById(long id) {
        if (ObjectUtils.isEmpty(id)){
            String uid = UUID.randomUUID().toString();
            log.error("ManageToDoImpl | deleteTodoById | traceID | " + uid);
            log.error("Error | " + "El parametro id es nulo o vacio");
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST.value())
                    .body(ErrorMessage.builder()
                            .code(ErrorCodes.GENERIC_ERROR.getMessage())
                            .exceptionType("N/A")
                            .message("El parametro id es nulo o vacio")
                            .traceID(uid)
                            .build());

        }
        try{
            iToDoRepository.deleteTodoById(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        catch (Exception e) {
            String uid = UUID.randomUUID().toString();
            log.error("ManageToDoImpl | deleteTodoById | traceID | " + uid);
            log.error("Exception | " + e);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .body(ErrorMessage.builder()
                            .code(ErrorCodes.GENERIC_ERROR.getMessage())
                            .exceptionType(e.getClass().toString())
                            .message(e.getMessage())
                            .traceID(uid)
                            .build());
        }
    }

    @Override
    public ResponseEntity getAllToDos() {

        try{
            List<ToDoDto> lista = iToDoRepository.getAllToDos();
            return ResponseEntity.ok(lista);
        }
        catch (Exception e) {
            String uid = UUID.randomUUID().toString();
            log.error("ManageToDoImpl | getAllToDos | traceID | " + uid);
            log.error("Exception | " + e);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .body(ErrorMessage.builder()
                            .code(ErrorCodes.GENERIC_ERROR.getMessage())
                            .exceptionType(e.getClass().toString())
                            .message(e.getMessage())
                            .traceID(uid)
                            .build());
        }
    }
}
