package com.itau.prueba.todoservices.repository.todo;

import com.google.firebase.database.*;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.itau.prueba.todoservices.adapter.manager.EndPointManager;
import com.itau.prueba.todoservices.common.dtos.ToDoDto;
import com.itau.prueba.todoservices.common.model.ToDo;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;


@Component
public class ToDoRepositoryImpl extends EndPointManager implements IToDoRepository {

    private DatabaseReference databaseReference;
    private List<ToDoDto> itemList;

    public ToDoRepositoryImpl() {
        itemList = new ArrayList<>();
    }

    @Override
    public List<ToDoDto> getTodoById(long id) {
        itemList.clear();
        String url = "https://todo-itau.firebaseio.com/item.json?orderBy=\"id\"&equalTo=" + id + "&print=pretty";

        HashMap<String, String> headers = new HashMap<>();
        headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        ResponseEntity response = endpointConsumerClient(url, String.class, HttpMethod.GET, headers);
        if (response.getBody().toString().equals("{ }\n")) {
            return null;
        }
        String json = response.getBody().toString();
        JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();

        Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
        for (Map.Entry<String, JsonElement> entry : entrySet) {
            JsonObject child = jsonObject.get(entry.getKey()).getAsJsonObject();
            ToDoDto item = new ToDoDto();
            item.setKey(entry.getKey());

            item.setId(Long.parseLong(child.get("id").toString()));
            item.setTitle(child.get("title").getAsString());
            itemList.add(item);
        }
        return itemList;
    }


    public List<ToDoDto> getAllToDos() {
        itemList.clear();
        String url = "https://todo-itau.firebaseio.com/item.json";

        HashMap<String, String> headers = new HashMap<>();
        headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        ResponseEntity response = endpointConsumerClient(url, String.class, HttpMethod.GET, headers);
        if (ObjectUtils.isEmpty(response.getBody())) {
            itemList.clear();
            return itemList;
        }
        String json = response.getBody().toString();
        JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();

        Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
        for (Map.Entry<String, JsonElement> entry : entrySet) {
            JsonObject child = jsonObject.get(entry.getKey()).getAsJsonObject();
            ToDoDto item = new ToDoDto();
            item.setKey(entry.getKey());

            item.setId(Long.parseLong(child.get("id").toString()));
            item.setTitle(child.get("title").getAsString());
            itemList.add(item);
        }
        return itemList;
    }


    @Override
    public List<ToDoDto> createTodo(ToDo item) throws Exception {
        CountDownLatch done = new CountDownLatch(1);
        List<ToDoDto> respuesta = new ArrayList<>();
        if (ObjectUtils.isEmpty(getTodoById(item.getId()))) {
            databaseReference = FirebaseDatabase.getInstance().getReference().child("item");
            databaseReference.push().setValue(item, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    respuesta.clear();
                    respuesta.addAll(getTodoById(item.getId()));
                    done.countDown();
                }
            });
        } else {
            respuesta.clear();
            done.countDown();
            throw new Exception("Item ya existente");
        }

        try {
            done.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return respuesta;
    }

    @Override
    public List<ToDoDto> updateTodoById(long id, ToDo updated) throws Exception {
        List<ToDoDto> item = getTodoById(id);
        CountDownLatch done = new CountDownLatch(1);

        if (!ObjectUtils.isEmpty(item)) {
            databaseReference = FirebaseDatabase.getInstance().getReference().child("item");
            Map<String, Object> data = new HashMap<>();
            data.put("id", updated.getId());
            data.put("title", updated.getTitle());
            databaseReference.child(item.get(0).getKey()).updateChildren(data, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    done.countDown();
                }
            });
        }else{
            done.countDown();
        }
        try {
            done.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return item;
    }

    @Override
    public synchronized void deleteTodoById(long id) throws Exception {
        CountDownLatch done = new CountDownLatch(1);
        List<ToDoDto> item = getTodoById(id);
        if (!ObjectUtils.isEmpty(item)) {
             FirebaseDatabase.getInstance().getReference().child("item").child(item.get(0).getKey()).setValue(null,new DatabaseReference.CompletionListener() {
                 @Override
                 public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                     done.countDown();
                 }
             });

        }else{
            done.countDown();
        }
        try {
            done.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
