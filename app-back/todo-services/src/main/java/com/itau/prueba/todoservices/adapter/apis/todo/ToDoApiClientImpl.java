package com.itau.prueba.todoservices.adapter.apis.todo;

import com.itau.prueba.todoservices.adapter.manager.EndPointManager;
import com.itau.prueba.todoservices.common.enums.ErrorCodes;
import com.itau.prueba.todoservices.common.model.ErrorMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.UUID;

@Component
public class ToDoApiClientImpl extends EndPointManager implements IToDoApiClient {

    private final Environment env;
    private static final Logger log = LogManager.getLogger(ToDoApiClientImpl.class);


    @Autowired
    public ToDoApiClientImpl(Environment env) {
        this.env = env;
    }

    @Override
    public ResponseEntity viewData() {

        HashMap<String, String> headers = new HashMap<>();
        headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        try {
            return endpointConsumerClient(env.getProperty("todo.api.endpoint"), String.class, HttpMethod.GET, headers);

        } catch (Exception e) {
            String uid = UUID.randomUUID().toString();
            log.error("ToDoApiClientImpl | viewData | traceID | " + uid);
            log.error("Exception | " + e);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .body(ErrorMessage.builder()
                            .code(ErrorCodes.GENERIC_ERROR.getMessage())
                            .exceptionType(e.getClass().toString())
                            .message(e.getMessage())
                            .traceID(uid)
                            .build());

        }

    }
}
