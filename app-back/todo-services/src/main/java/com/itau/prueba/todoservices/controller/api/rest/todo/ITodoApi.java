package com.itau.prueba.todoservices.controller.api.rest.todo;

import com.itau.prueba.todoservices.common.model.ToDo;
import org.springframework.http.ResponseEntity;

public interface ITodoApi {

    ResponseEntity viewData();
    ResponseEntity getTodoById(long id);
    ResponseEntity createTodo(ToDo item);
    ResponseEntity updateTodoById(ToDo item, long id);
    ResponseEntity deleteTodoById(long id);
    ResponseEntity getAllToDos();

}
