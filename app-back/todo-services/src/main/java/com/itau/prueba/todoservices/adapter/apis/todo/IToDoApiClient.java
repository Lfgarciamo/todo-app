package com.itau.prueba.todoservices.adapter.apis.todo;

import org.springframework.http.ResponseEntity;

public interface IToDoApiClient {
    ResponseEntity viewData();
}
