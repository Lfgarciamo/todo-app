package com.itau.prueba.todoservices.services.todo;

import com.itau.prueba.todoservices.common.model.ToDo;
import org.springframework.http.ResponseEntity;

public interface IManageToDo {

    ResponseEntity getTodoById(long id);
    ResponseEntity createTodo(ToDo item);
    ResponseEntity updateTodoById(ToDo item, long id);
    ResponseEntity deleteTodoById(long id);
    ResponseEntity getAllToDos();
}
