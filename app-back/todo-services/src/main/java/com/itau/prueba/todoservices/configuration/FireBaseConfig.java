package com.itau.prueba.todoservices.configuration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;

@Configuration
public class FireBaseConfig {

    @Autowired
    ResourceLoader resourceLoader;

    @Bean
    FirebaseApp createFireBaseApp() throws IOException{
        Resource resource = new ClassPathResource("static/todo-itau-firebase-adminsdk-nfrd9-d9a145fe2d.json");
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream( resource.getInputStream()))
                .setDatabaseUrl("https://todo-itau.firebaseio.com/")
                .build();

        return  FirebaseApp.initializeApp(options);
    }




}
